package fr.univ_fcomte.iut_bm.tetris.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigManager {
  private static final ConfigManager theInstance = new ConfigManager();

  private Config config = new Config();

  public File getConfigFile() {
    Path config = Paths.get(System.getProperty("user.home"),
        ".config", "tetris-project", "config.json");

    if (isWindows()) {
      config = Paths.get(System.getenv("APPDATA"), "tetris-project", "config.json");
    }

    return config.toFile();
  }

  public void load() throws FileNotFoundException {
    load(getConfigFile());
  }

  public void load(File file) throws FileNotFoundException {
    Gson gson = new Gson();
    config = gson.fromJson(new FileReader(file), Config.class);
  }

  public void save() throws IOException {
    save(getConfigFile());
  }

  public void save(File file) throws IOException {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    //noinspection ResultOfMethodCallIgnored
    file.getParentFile().mkdirs();
    FileWriter writer = new FileWriter(file);
    gson.toJson(config, writer);
    writer.close();
  }

  public Config getConfig() {
    return config;
  }

  private boolean isWindows() {
    return System.getProperty("os.name").toLowerCase().contains("win");
  }

  public static ConfigManager getInstance() {
    return theInstance;
  }
}

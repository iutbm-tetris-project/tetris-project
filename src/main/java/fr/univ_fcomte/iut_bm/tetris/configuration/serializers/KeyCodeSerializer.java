package fr.univ_fcomte.iut_bm.tetris.configuration.serializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import javafx.scene.input.KeyCode;

public class KeyCodeSerializer implements JsonSerializer<KeyCode>, JsonDeserializer<KeyCode> {

  @Override
  public JsonElement serialize(KeyCode src, Type typeOfSrc, JsonSerializationContext context) {
    return new JsonPrimitive(src.getName());
  }

  @Override
  public KeyCode deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    return KeyCode.getKeyCode(json.getAsString());
  }
}

package fr.univ_fcomte.iut_bm.tetris.configuration;

import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import javafx.scene.input.KeyCode;

public class Config {
  private Map<KeyCode, Command> inputMapping = Map.ofEntries(
      new SimpleEntry<>(KeyCode.LEFT, Command.LEFT),
      new SimpleEntry<>(KeyCode.RIGHT, Command.RIGHT),
      new SimpleEntry<>(KeyCode.DOWN, Command.DOWN),
      new SimpleEntry<>(KeyCode.SPACE, Command.DROP),
      new SimpleEntry<>(KeyCode.Z, Command.ROTATE_BACK),
      new SimpleEntry<>(KeyCode.UP, Command.ROTATE),
      new SimpleEntry<>(KeyCode.C, Command.HOLD),
      new SimpleEntry<>(KeyCode.X, Command.ACTIVATE_BM)
  );

  public Map<KeyCode, Command> getInputMapping() {
    return inputMapping;
  }

  public void setInputMapping(
      Map<KeyCode, Command> inputMapping) {
    this.inputMapping = inputMapping;
  }
}

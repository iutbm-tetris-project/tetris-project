package fr.univ_fcomte.iut_bm.tetris;

import fr.univ_fcomte.iut_bm.tetris.configuration.ConfigManager;
import fr.univ_fcomte.iut_bm.tetris.controllers.BasicController;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeSceneEvent;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.Style;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class App extends Application {

  private final String DEFAULT_SCENE = "MainMenu";
  private final int WIDTH = 1280, HEIGHT = 720;

  private JMetro theme;

  private static BasicController controller;

  private Stage stage;

  public static void loadConfig() throws IOException {
    ConfigManager configManager = ConfigManager.getInstance();
    if (configManager.getConfigFile().exists()) {
      configManager.load();
    } else {
      configManager.save();
      configManager.load();
    }
  }

  public static void main(String[] args) throws IOException {
    loadConfig();
    launch(args);
  }

  @Override
  public void start(Stage stage) {
    theme = new JMetro(Style.LIGHT);
    theme.setAutomaticallyColorPanes(true);

    this.stage = stage;

    EventBus.getDefault().register(this);

    stage.setMinHeight(WIDTH);
    stage.setWidth(WIDTH);
    stage.setMaxHeight(WIDTH);

    stage.setMinHeight(HEIGHT);
    stage.setHeight(HEIGHT);
    stage.setMaxHeight(HEIGHT);

    stage.setTitle("Tetris");
    stage.setResizable(false);
    stage.show();

    EventBus.getDefault().post(new ChangeSceneEvent(DEFAULT_SCENE));
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onChangeScene(ChangeSceneEvent e) throws IOException {
    if (controller != null)
      controller.onStop();

    Parent root = FXMLLoader.load(
        getClass().getResource(
            String.format("/fr/univ_fcomte/iut_bm/tetris/scenes/%s.fxml", e.getSceneName())
        )
    );
    Scene scene = new Scene(root);
    theme.setScene(scene);
    stage.setScene(scene);

    controller.onStart(e.getData());
  }

  public static void setController(BasicController controller) {
    App.controller = controller;
  }
}

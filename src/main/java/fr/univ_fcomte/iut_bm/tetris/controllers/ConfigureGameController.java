package fr.univ_fcomte.iut_bm.tetris.controllers;

import fr.univ_fcomte.iut_bm.tetris.game.primitives.Configuration;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeSceneEvent;
import fr.univ_fcomte.iut_bm.tetris.game.generators.Generator;
import fr.univ_fcomte.iut_bm.tetris.game.generators.LegacyGenerator;
import fr.univ_fcomte.iut_bm.tetris.game.generators.ModernGenerator;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.greenrobot.eventbus.EventBus;

public class ConfigureGameController extends BasicController {

  @FXML
  private ComboBox<String> generatorComboBox;

  @FXML
  private TextField seedField;

  @FXML
  private TextField startLevelField;

  private Map<String, Class<? extends Generator>> generators;

  @Override
  public void onStart(Object data) {
    generators = Map.ofEntries(
        new SimpleEntry<>("Modern generator", ModernGenerator.class),
        new SimpleEntry<>("Legacy generator (NES)", LegacyGenerator.class)
    );

    generatorComboBox.getItems().addAll(generators.keySet());
    generatorComboBox.getSelectionModel().selectFirst();

    seedField.focusedProperty().addListener((x, focus, y) -> {
      if (focus) {
        String text = seedField.getText().toLowerCase();

        try {
          Long.valueOf(text, 36);
        } catch (NumberFormatException e) {
          text = String.valueOf(System.currentTimeMillis());
        }

        seedField.setText(text);
      }
    });

    startLevelField.focusedProperty().addListener((x, focus, y) -> {
      if (focus) {
        String text = startLevelField.getText();

        try {
          Integer.parseInt(text);
        } catch (NumberFormatException e) {
          text = "0";
        }

        startLevelField.setText(text);
      }
    });

    startLevelField.setText("0");
    seedField.setText(String.valueOf(System.currentTimeMillis()));
  }

  @Override
  public void onStop() {

  }

  public void startGame() {
    Configuration config = new Configuration(generators.get(generatorComboBox.getValue()),
        Long.valueOf(seedField.getText(), 36), Integer.parseInt(startLevelField.getText()));
    EventBus.getDefault().post(new ChangeSceneEvent("SinglePlayer", config));
  }
}

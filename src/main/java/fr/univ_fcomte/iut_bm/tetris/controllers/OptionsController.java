package fr.univ_fcomte.iut_bm.tetris.controllers;

import fr.univ_fcomte.iut_bm.tetris.configuration.Config;
import fr.univ_fcomte.iut_bm.tetris.configuration.ConfigManager;
import fr.univ_fcomte.iut_bm.tetris.game.Utilities;
import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeSceneEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import org.greenrobot.eventbus.EventBus;

public class OptionsController extends BasicController {

  private ConfigManager configManager = ConfigManager.getInstance();
  private Config config;

  private Map<TextField, Command> textFieldMap = new HashMap<>();

  @FXML
  private GridPane controlsGrid;

  @Override
  public void onStart(Object data) {
    config = configManager.getConfig();

    Arrays.stream(Command.values()).forEach((cmd) -> {
      KeyCode key = Utilities.inverseMap(config.getInputMapping()).get(cmd);
      int row = controlsGrid.getRowCount() + 1;

      Label label = new Label(cmd.getName());
      TextField textField = new TextField();

      textField.setOnKeyPressed(this::changeKey);
      textField.addEventFilter(KeyEvent.KEY_TYPED, KeyEvent::consume);

      controlsGrid.add(label, 0, row);
      controlsGrid.add(textField, 1, row);

      textFieldMap.put(textField, cmd);
    });

    update();
  }

  @Override
  public void onStop() {
  }

  private void changeKey(KeyEvent e) {
    Map<Command, KeyCode> inverseMapping = Utilities.inverseMap(config.getInputMapping());

    KeyCode code = e.getCode();

    TextField textField = (TextField) e.getSource();
    KeyCode oldCode = KeyCode.getKeyCode(textField.getText());

    Command cmd = textFieldMap.get(textField);

    KeyCode prevCode = inverseMapping.get(cmd);
    config.getInputMapping().remove(prevCode);

    if (config.getInputMapping().containsKey(code)) {
      Command other = config.getInputMapping().get(code);
      config.getInputMapping().put(oldCode, other);
    }

    config.getInputMapping().put(code, cmd);
    update();
  }

  private void update() {
    Map<Command, TextField> textFields = Utilities.inverseMap(textFieldMap);

    for (Entry<KeyCode, Command> entry : config.getInputMapping().entrySet()) {
      TextField textField = textFields.get(entry.getValue());
      textField.setText(entry.getKey().getName());
    }
  }

  public void apply() throws IOException {
    configManager.save();
    EventBus.getDefault().post(new ChangeSceneEvent("MainMenu"));
  }
}

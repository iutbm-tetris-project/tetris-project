package fr.univ_fcomte.iut_bm.tetris.controllers;

import fr.univ_fcomte.iut_bm.tetris.game.primitives.Statistic;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeSceneEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.greenrobot.eventbus.EventBus;

public class GameOverController extends BasicController {

  @FXML
  private Label scoreLabel;
  @FXML
  private Label levelLabel;

  @Override
  public void onStart(Object data) {
    Statistic stats = (Statistic) data;
    scoreLabel.setText(String.format("%016d", stats.getScore()));
    levelLabel.setText(String.format("%02d", stats.getLevel()));
  }

  @Override
  public void onStop() {

  }

  public void goMenu() {
    EventBus.getDefault().post(new ChangeSceneEvent("MainMenu"));
  }
}

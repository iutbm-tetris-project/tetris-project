package fr.univ_fcomte.iut_bm.tetris.controllers;

import fr.univ_fcomte.iut_bm.tetris.game.events.KeyStateChangeEvent;
import java.util.ArrayList;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.greenrobot.eventbus.EventBus;

public abstract class BasicGameController extends BasicController {

  protected ArrayList<KeyCode> keys;

  public BasicGameController() {
    keys = new ArrayList<>();
  }

  public void onKeyReleased(KeyEvent e) {
    EventBus.getDefault().post(new KeyStateChangeEvent(e.getCode(), false));
  }

  public void onKeyPressed(KeyEvent e) {
    EventBus.getDefault().post(new KeyStateChangeEvent(e.getCode(), true));
  }
}

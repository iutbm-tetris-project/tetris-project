package fr.univ_fcomte.iut_bm.tetris.controllers;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeSceneEvent;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import org.greenrobot.eventbus.EventBus;

public class HelpController extends BasicController {

  public static final String HELP_DOC_LOCATION = "/fr/univ_fcomte/iut_bm/tetris/doc/help.html";

  @FXML
  private VBox box;

  @Override
  public void onStart(Object data) {
    WebView webView = new WebView();
    webView.getEngine().load(getClass().getResource(HELP_DOC_LOCATION).toString());

    ScrollPane scrollPane = new ScrollPane();
    scrollPane.setFitToWidth(true);
    scrollPane.setFitToHeight(true);
    scrollPane.setContent(webView);

    box.getChildren().add(webView);
  }

  @Override
  public void onStop() {

  }

  public void goMenu() {
    EventBus.getDefault().post(new ChangeSceneEvent("MainMenu"));
  }
}

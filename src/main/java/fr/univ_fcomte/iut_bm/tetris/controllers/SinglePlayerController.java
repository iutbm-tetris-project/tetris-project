package fr.univ_fcomte.iut_bm.tetris.controllers;

import fr.univ_fcomte.iut_bm.tetris.game.primitives.Configuration;
import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.generators.Generator;
import fr.univ_fcomte.iut_bm.tetris.game.generators.ModernGenerator;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

public class SinglePlayerController extends BasicGameController {

  private Random rand = new Random();

  @FXML
  private Canvas colorCanvas;
  @FXML
  private Canvas blockCanvas;

  @FXML
  private ImageView holdBox;
  @FXML
  private ImageView bonusMalus;

  @FXML
  private ImageView nextBox1;
  @FXML
  private ImageView nextBox2;
  @FXML
  private ImageView nextBox3;
  @FXML
  private ImageView nextBox4;
  @FXML
  private ImageView nextBox5;

  @FXML
  private Label scoreLabel;
  @FXML
  private Label levelLabel;

  private TetrisGame game;

  private GraphicsContext blockGC;
  private GraphicsContext colorGC;

  public void onStart(Object data) {
    if (data == null)
      throw new RuntimeException("data is null");

    if (!(data instanceof Configuration))
      throw new RuntimeException("data isn't a Configuration object");

    Configuration config = (Configuration) data;

    blockCanvas.setFocusTraversable(true);
    blockCanvas.requestFocus();

    blockGC = blockCanvas.getGraphicsContext2D();
    colorGC = colorCanvas.getGraphicsContext2D();

    Generator generator = new ModernGenerator(config.getSeed());

    try {
      Constructor<? extends Generator> generatorConstructor =
          config.getGeneratorClass().getConstructor(Long.class);
      generator = generatorConstructor.newInstance(config.getSeed());
    } catch (NoSuchMethodException | IllegalAccessException |
        InstantiationException | InvocationTargetException e) {
      // TODO: Handle oops
    }

    game = new TetrisGame(
        generator,
        config.getLevelStart(),
        new ImageView[]{
            nextBox1,
            nextBox2,
            nextBox3,
            nextBox4,
            nextBox5
        },
        holdBox,
        bonusMalus,
        blockGC,
        colorGC,
        scoreLabel,
        levelLabel
    );
    game.start();
  }

  public void onStop() {
    game.stop();
  }
}

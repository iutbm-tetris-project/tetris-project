package fr.univ_fcomte.iut_bm.tetris.controllers;

import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeSceneEvent;
import javafx.application.Platform;
import org.greenrobot.eventbus.EventBus;

public class MainMenuController extends BasicController {

  public void onStart(Object data) {}
  public void onStop() {}

  public void startGame() {
    EventBus.getDefault().post(new ChangeSceneEvent("ConfigureGame"));
  }

  public void options() {
    EventBus.getDefault().post(new ChangeSceneEvent("Options"));
  }

  public void help() {
    EventBus.getDefault().post(new ChangeSceneEvent("Help"));
  }

  public void exit() {
    Platform.exit();
  }
}

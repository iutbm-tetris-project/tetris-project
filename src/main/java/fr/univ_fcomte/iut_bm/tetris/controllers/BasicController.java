package fr.univ_fcomte.iut_bm.tetris.controllers;

import fr.univ_fcomte.iut_bm.tetris.App;

public abstract class BasicController {
  public void initialize() {
    App.setController(this);
  }

  public abstract void onStart(Object data);
  public abstract void onStop();
}

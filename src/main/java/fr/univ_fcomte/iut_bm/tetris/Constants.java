package fr.univ_fcomte.iut_bm.tetris;

import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import javafx.scene.image.Image;

public class Constants {

  public static final double BLOCK_SIZE = 32;

  public static Image BLOCK_IMAGE = new Image(Constants.class
      .getResource("/fr/univ_fcomte/iut_bm/tetris/textures/Block.png").toString());

  public static int[] FRAME_DELAYS = new int[] {
      48,
      43, 38, 33, 28, 23,
      18, 13, 8, 6, 5, 5,
       5,  4, 4, 4, 3, 3,
       3,  2, 2, 2, 2, 2,
       2,  2, 2, 2, 2, 1
  };
  public static int[] LINE_POINTS = new int[]{0, 40, 100, 300, 1200};
}

package fr.univ_fcomte.iut_bm.tetris.game.components;

import static fr.univ_fcomte.iut_bm.tetris.Constants.BLOCK_IMAGE;
import static fr.univ_fcomte.iut_bm.tetris.Constants.BLOCK_SIZE;
import static fr.univ_fcomte.iut_bm.tetris.game.components.Field.FIELD_HEIGHT;
import static fr.univ_fcomte.iut_bm.tetris.game.components.Field.FIELD_WIDTH;
import static fr.univ_fcomte.iut_bm.tetris.game.components.Field.FILED_TRUE_HEIGHT;

import fr.univ_fcomte.iut_bm.tetris.game.BlockRenderer;
import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.Objects;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MovablePiece extends Piece implements BlockRenderer {

  private Point pos;
  private Field field;

  public MovablePiece() {
    this(new Point());
  }

  public MovablePiece(Point pos) {
    super();
    this.pos = pos;
  }

  public MovablePiece(MovablePiece mb) {
    this(mb.getShape(), mb.getColor(), mb.pos, mb.field);
  }

  public MovablePiece(Piece piece, Point pos, Field field) {
    super(piece);
    this.pos = pos;
    this.field = field;
  }

  public MovablePiece(PieceEnum type, Point pos, Field field) {
    super(type);
    this.pos = pos;
    this.field = field;
  }

  public MovablePiece(Point[] shape, Color col, Point pos, Field field) {
    super(shape, col);
    this.pos = pos;
    this.field = field;
  }

  private boolean checkOutOfBounds(Point point) {
    return point.getX() < 0 ||
        point.getX() >= FIELD_WIDTH ||
        point.getY() < 0 ||
        point.getY() >= FILED_TRUE_HEIGHT;
  }

  public boolean fit() {
    Point[] shape = this.getShapePosition();
    Color[][] colors = field.getColors();

    for (Point point : shape) {
        if (checkOutOfBounds(point)) {
            return false;
        }
        if (colors[point.getX()][point.getY()] != null) {
            return false;
        }
    }
    return true;
  }

  public void attach() {
    Point[] shape = this.getShapePosition();
    Color[][] colors = field.getColors();

      for (Point point : shape) {
          colors[point.getX()][point.getY()] = col;
      }
  }

  private boolean tryToGo(int times, Mover direction, Mover oppositeDirection) {
    direction.move(times);
      if (fit()) {
          return true;
      }
    oppositeDirection.move(times);
    return false;
  }

  public boolean tryToGoUp() {
    return tryToGoUp(1);
  }

  public boolean tryToGoUp(int times) {
    return tryToGo(times, this::goUp, this::goDown);
  }

  public boolean tryToGoDown() {
    return tryToGoDown(1);
  }

  public boolean tryToGoDown(int times) {
    return tryToGo(times, this::goDown, this::goUp);
  }

  public boolean tryToGoRight() {
    return tryToGoRight(1);
  }

  public boolean tryToGoRight(int times) {
    return tryToGo(times, this::goRight, this::goLeft);
  }

  public boolean tryToGoLeft() {
    return tryToGoLeft(1);
  }

  public boolean tryToGoLeft(int times) {
    return tryToGo(times, this::goLeft, this::goRight);
  }

  public boolean tryToRotate() {
    return tryToRotate(1);
  }

  public boolean tryToRotate(int times) {
    return tryToGo(times, this::rotate, this::rotateBack);
  }

  public boolean tryToRotateBack() {
    return tryToRotateBack(1);
  }

  public boolean tryToRotateBack(int times) {
    return tryToGo(times, this::rotateBack, this::rotate);
  }

  public void goUp(int times) {
    pos = pos.add(0, times);
  }

  public void goDown(int times) {
    pos = pos.subtract(0, times);
  }

  public void goRight(int times) {
    pos = pos.add(times, 0);
  }

  public void goLeft(int times) {
    pos = pos.subtract(times, 0);
  }

  @SuppressWarnings("StatementWithEmptyBody")
  public void drop() {
      while (tryToGoDown()) {
      }
  }

  public void rotate(int times) {
      if(super.spin) {
        Point[] shape = super.getShape();
        for (int j = 0; j < times; j++) {
          for (int k = 0; k < shape.length; k++) {
            shape[k] = new Point(-shape[k].getY(), shape[k].getX());
          }
        }
        super.setShape(shape);
      }
  }

  public void rotateBack(int times) {
    if(super.spin) {
      Point[] shape = super.getShape();
      for (int j = 0; j < times; j++) {
        for (int k = 0; k < shape.length; k++) {
          shape[k] = new Point(shape[k].getY(), -shape[k].getX());
        }
      }
      super.setShape(shape);
    }
  }

  @Override
  public boolean equals(Object o) {
      if (this == o) {
          return true;
      }
      if (o == null || getClass() != o.getClass()) {
          return false;
      }
      if (!super.equals(o)) {
          return false;
      }
    MovablePiece that = (MovablePiece) o;
    return Objects.equals(pos, that.pos);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pos);
  }

  public Point[] getShapePosition() {
    Point[] shapePosition = super.getShape().clone();
    for (int i = 0; i < shapePosition.length; i++) {
      shapePosition[i] = shapePosition[i].add(this.pos);
    }
    return shapePosition;
  }

  public Point getPosition() {
    return pos;
  }

  public void setPosition(Point pos) {
    this.pos = pos;
  }

  @Override
  public void renderBlocks(GraphicsContext gc) {
    for (Point block : getShapePosition()) {
      Point2D truePoint = block.toPoint2D();
      gc.drawImage(BLOCK_IMAGE, truePoint.getX(), truePoint.getY());
    }
  }

  @Override
  public void renderColor(GraphicsContext gc) {
    gc.save();
    gc.setFill(col);
    for (Point block : getShapePosition()) {
      Point2D truePoint = block.toPoint2D();
      gc.fillRect(truePoint.getX(), truePoint.getY(), BLOCK_SIZE, BLOCK_SIZE);
    }
    gc.restore();
  }

  private interface Mover {

    void move(int times);
  }
}

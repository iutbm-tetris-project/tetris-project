package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.components.MovablePiece;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import fr.univ_fcomte.iut_bm.tetris.game.events.ActionEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.MoveEvent;
import org.greenrobot.eventbus.Subscribe;

public class MovementSystem {

  @Subscribe(priority = EventPriority.HIGH)
  public void onMove(MoveEvent e) {
    TetrisGame tg = TetrisGame.getInstance();

    if (tg == null) {
      return;
    }

    Command command = e.getCommand();
    MovablePiece piece = tg.getPlayer().getPiece();

    switch (command) {
      case LEFT:
        piece.tryToGoLeft();
        break;
      case RIGHT:
        piece.tryToGoRight();
        break;
      case DOWN:
        piece.tryToGoDown();
        break;
    }
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onAction(ActionEvent e) {
    TetrisGame tg = TetrisGame.getInstance();

    if (tg == null) {
      return;
    }

    Command command = e.getCommand();
    MovablePiece piece = tg.getPlayer().getPiece();

    switch (command) {
      case ROTATE:
        piece.tryToRotate();
        break;
      case ROTATE_BACK:
        piece.tryToRotateBack();
        break;
      case DROP:
        piece.drop();
        break;
    }
  }
}

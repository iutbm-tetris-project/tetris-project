package fr.univ_fcomte.iut_bm.tetris.game.systems;


import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.CommandType;
import fr.univ_fcomte.iut_bm.tetris.game.events.MoveEvent;
import java.util.ArrayList;
import javafx.scene.input.KeyCode;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class DelayedAutoShiftSystem {

  public static int[] DAS_PROFILE = new int[]{1, 16, 6};

  private ArrayList<KeyCode> lastKeyStates;
  private long lastKeyStatesFrame;
  private int dasCounter = 0;

  @Subscribe(priority = EventPriority.HIGHER)
  public void onMove(MoveEvent e) {
    ArrayList<KeyCode> keyStates = e.getKeyStates();
    int frameCount = e.getFrameCount();

    if (frameCount == lastKeyStatesFrame) {
      return;
    }

    if (lastKeyStates == null || !lastKeyStates.equals(keyStates)) {
      lastKeyStates = keyStates;
      dasCounter = 0;
    } else {
      int delay = DAS_PROFILE[DAS_PROFILE.length - 1];

      if (dasCounter < DAS_PROFILE.length) {
        delay = DAS_PROFILE[dasCounter];
      }

      if (frameCount - lastKeyStatesFrame < delay) {
        EventBus.getDefault().cancelEventDelivery(e);
        return;
      }
    }

    dasCounter++;
    lastKeyStatesFrame = frameCount;
  }
}

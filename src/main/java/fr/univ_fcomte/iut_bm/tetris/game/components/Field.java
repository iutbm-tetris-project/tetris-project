package fr.univ_fcomte.iut_bm.tetris.game.components;

import static fr.univ_fcomte.iut_bm.tetris.Constants.BLOCK_IMAGE;
import static fr.univ_fcomte.iut_bm.tetris.Constants.BLOCK_SIZE;
import static fr.univ_fcomte.iut_bm.tetris.game.Utilities.translateToRealPoint;

import fr.univ_fcomte.iut_bm.tetris.game.BlockRenderer;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Field implements BlockRenderer {

  public static int FIELD_WIDTH = 10;
  public static int FIELD_HEIGHT = 20;
  public static int FILED_TRUE_HEIGHT = FIELD_HEIGHT + 4;

  private Color[][] colors;
  private Point spawnPoint;

  public Field() {
    colors = new Color[FIELD_WIDTH][FILED_TRUE_HEIGHT];
    calculateSpawnPoint();
  }

  public Field(Field f) {
    this(f.colors, f.spawnPoint);
  }

  public Field(Color[][] colors) {
    this.colors = colors;
    this.calculateSpawnPoint();
  }

  public Field(Color[][] colors, Point spawnPoint) {
    this.colors = colors;
    this.spawnPoint = spawnPoint;
  }

  private void calculateSpawnPoint() {
    spawnPoint = new Point(FIELD_WIDTH / 2, FIELD_HEIGHT);
  }

  public void renderBlocks(GraphicsContext gc) {
    for (int x = 0; x < colors.length; x++) {
      for (int y = 0; y < colors[x].length; y++) {
        if (colors[x][y] != null) {
          Point2D realPoint = translateToRealPoint(x, y);
          gc.drawImage(BLOCK_IMAGE, realPoint.getX(), realPoint.getY());
        }
      }
    }
  }

  public void renderColor(GraphicsContext gc) {
    gc.save();
    for (int x = 0; x < colors.length; x++) {
      for (int y = 0; y < colors[x].length; y++) {
        Color color = colors[x][y];
        if (color != null) {
          Point2D realPoint = translateToRealPoint(x, y);
          gc.setFill(color);
          gc.fillRect(realPoint.getX(), realPoint.getY(), BLOCK_SIZE, BLOCK_SIZE);
        }
      }
    }
    gc.restore();
  }


  public boolean checkLine(int y) {
      for (int x = 0; x < FIELD_WIDTH; x++) {
          if (colors[x][y] == null) {
              return false;
          }
      }
    return true;
  }

  public boolean checkEmptyLine(int y) {
      for (int x = 0; x < FIELD_WIDTH; x++) {
          if (colors[x][y] != null) {
              return false;
          }
      }
    return true;
  }

  public void clearLine(int y) {
      for (int x = 0; x < FIELD_WIDTH; x++) {
          colors[x][y] = null;
      }
  }

  public void moveLine(int y1, int y2) {
    boolean[] copyBlocks = new boolean[FIELD_WIDTH];
    Color[] copyColors = new Color[FIELD_WIDTH];

      for (int x = 0; x < FIELD_WIDTH; x++) {
          copyColors[x] = colors[x][y1];
      }

    clearLine(y1);

      for (int x = 0; x < FIELD_WIDTH; x++) {
          colors[x][y2] = copyColors[x];
      }
  }

  public void shiftLine(int y) {
    moveLine(y, y - 1);
  }

  public void gravity() {
    for (int y = 1; y < FIELD_HEIGHT; y++) {
      for (int pos = y; pos > 0; pos--) {
          if (!checkEmptyLine(pos - 1)) {
              break;
          }
        shiftLine(pos);
      }
    }
  }

  public int clearLines() {
    int lines = 0;
    for (int y = 0; y < FIELD_HEIGHT; y++) {
      if (checkLine(y)) {
        clearLine(y);
        lines++;
      }
    }
    return lines;
  }

  public int countLines() {
    int lines = 0;
    for (int y = 0; y < FIELD_HEIGHT; y++) {
      if (checkLine(y)) {
        lines++;
      }
    }
    return lines;
  }

  public Point getSpawnPoint() {
    return spawnPoint;
  }

  public void setSpawnPoint(Point spawnPoint) {
    this.spawnPoint = spawnPoint;
  }

  public Color[][] getColors() {
    return colors;
  }

  public void setColors(Color[][] colors) {
    this.colors = colors;
  }
}

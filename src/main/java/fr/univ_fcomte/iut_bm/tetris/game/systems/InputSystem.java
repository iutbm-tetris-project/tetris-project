package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.configuration.ConfigManager;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import fr.univ_fcomte.iut_bm.tetris.game.enums.CommandType;
import fr.univ_fcomte.iut_bm.tetris.game.events.ActionEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameUpdateEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.KeyStateChangeEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.MoveEvent;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.scene.input.KeyCode;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class InputSystem {

  private Map<KeyCode, Command> inputMapping;
  private List<Command> heldActions = new ArrayList<>();

  public InputSystem() {
    ConfigManager configManager = ConfigManager.getInstance();
    inputMapping = configManager.getConfig().getInputMapping();
  }

  @Subscribe(priority = EventPriority.HIGHEST)
  public void onGameUpdate(GameUpdateEvent e) {
    for (KeyCode key : e.getKeyStates()) {
      if (inputMapping.containsKey(key)) {
        Command command = inputMapping.get(key);

        if (command.getType() == CommandType.TRANSLATION) {
          EventBus.getDefault().post(
              new MoveEvent(
                  e.getFrameCount(),
                  command,
                  e.getKeyStates()
              )
          );
        }
      }
    }
  }

  @Subscribe
  public void onKeyStateChange(KeyStateChangeEvent e) {
    if (!inputMapping.containsKey(e.getKey()))
      return;

    Command command = inputMapping.get(e.getKey());

    if (command.getType() != CommandType.ACTION) {
      return;
    }

    if (e.isPressed()) {
      if (!heldActions.contains(command)) {
        EventBus.getDefault().post(new ActionEvent(command));
        heldActions.add(command);
      }
    } else {
      heldActions.remove(command);
    }
  }
}

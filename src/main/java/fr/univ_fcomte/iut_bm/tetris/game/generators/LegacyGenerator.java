package fr.univ_fcomte.iut_bm.tetris.game.generators;

import static fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum.TETRIS_PIECES;

import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.ArrayList;
import java.util.List;

public class LegacyGenerator extends Generator {

  public LegacyGenerator(long seed) {
    super(seed);
  }

  @Override
  public List<PieceEnum> generateBlocks() {
    List<PieceEnum> pieces = new ArrayList<>();

    for (int i = 0; i < TETRIS_PIECES.length; i++) {
      pieces.add(TETRIS_PIECES[Math.abs(random.nextInt()) % TETRIS_PIECES.length]);
    }

    return pieces;
  }
}

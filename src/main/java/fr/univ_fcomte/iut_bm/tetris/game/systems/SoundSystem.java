package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import fr.univ_fcomte.iut_bm.tetris.game.events.ActionEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.BonusActivateEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.BonusReadyEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameStartEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.LineClearEvent;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.media.AudioClip;
import org.greenrobot.eventbus.Subscribe;

public class SoundSystem {
  public static final String SOUND_DIR = "/fr/univ_fcomte/iut_bm/tetris/sound/";

  public static final String[] SOUND_FILES = {
      "bonus_activate",
      "bonus_ready",
      "hard_drop",
      "line_clear"
  };

  private Map<String, AudioClip> audioMap = new HashMap<>();

  @Subscribe
  public void onStart(GameStartEvent e) {
    loadSounds();
  }

  @Subscribe
  public void onLineClear(LineClearEvent e) {
    playSound("line_clear");
  }

  @Subscribe
  public void onAction(ActionEvent e) {
    if (e.getCommand() == Command.DROP) {
      playSound("hard_drop");
    }
  }

  @Subscribe
  public void onBonusReady(BonusReadyEvent e) {
    playSound("bonus_ready");
  }

  @Subscribe
  public void onBonusActivate(BonusActivateEvent e) {
    playSound("bonus_activate");
  }

  public void loadSounds() {
    for (String soundFile : SOUND_FILES) {
      audioMap.put(
          soundFile,
          new AudioClip(getClass().getResource(SOUND_DIR + soundFile + ".mp3").toString())
      );
    }
  }

  public void playSound(String sound) {
    audioMap.get(sound).play();
  }
}

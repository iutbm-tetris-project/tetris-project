package fr.univ_fcomte.iut_bm.tetris.game.bonus_malus;

import org.greenrobot.eventbus.Subscribe;

public abstract class BonusMalus {

  public abstract void onEnable();

  public abstract void onDisable();

  @Subscribe
  public void ignoreMe(int i) {
  }
}

package fr.univ_fcomte.iut_bm.tetris.game.primitives;

import fr.univ_fcomte.iut_bm.tetris.game.bonus_malus.BonusMalus;
import fr.univ_fcomte.iut_bm.tetris.game.generators.Generator;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class BonusMalusEntry {
  private final String name;
  private final String icon;
  private final Class<? extends BonusMalus> bonusMalusClass;
  private final int duration;

  public BonusMalusEntry(String name, String icon,
      Class<? extends BonusMalus> bonusMalusClass, int duration) {
    this.name = name;
    this.icon = icon;
    this.bonusMalusClass = bonusMalusClass;
    this.duration = duration;
  }

  public String getName() {
    return name;
  }

  public String getIcon() {
    return icon;
  }

  public Class<? extends BonusMalus> getBonusMalusClass() {
    return bonusMalusClass;
  }

  public int getDuration() {
    return duration;
  }

  public BonusMalus constructBonusMalus() {
    try {
      Constructor<? extends BonusMalus> constructor = bonusMalusClass.getConstructor();
      return constructor.newInstance();
    } catch (NoSuchMethodException | IllegalAccessException |
        InstantiationException | InvocationTargetException e) {
      return null;
    }
  }
}

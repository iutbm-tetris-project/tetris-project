package fr.univ_fcomte.iut_bm.tetris.game.generators;

import static fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum.TETRIS_PIECES;

import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ModernGenerator extends Generator {

  public ModernGenerator(long seed) {
    super(seed);
  }

  @Override
  public List<PieceEnum> generateBlocks() {
    List<PieceEnum> pieces = Arrays.asList(TETRIS_PIECES);
    Collections.shuffle(pieces, random);
    return pieces;
  }
}

package fr.univ_fcomte.iut_bm.tetris.game.bonus_malus;

import static fr.univ_fcomte.iut_bm.tetris.Constants.FRAME_DELAYS;

import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.components.MovablePiece;
import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameUpdateEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.InvokeNextPieceEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class Speed extends BonusMalus {

  @Override
  public void onEnable() {
    EventBus.getDefault().register(this);
  }

  @Override
  public void onDisable() {
    EventBus.getDefault().unregister(this);
  }

  @Subscribe(priority = EventPriority.LOWER)
  public void onUpdate(GameUpdateEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();
    MovablePiece currentPiece = player.getPiece();

    int delayIndex = Math.min(player.getLevel(), 29);
    int delay = FRAME_DELAYS[delayIndex];

    if (e.getFrameCount() % delay == 0) {
      currentPiece.tryToGoDown();
    }
  }
}

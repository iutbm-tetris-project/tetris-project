package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeScoreEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameStartEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextLevelEvent;
import javafx.scene.control.Label;
import org.greenrobot.eventbus.Subscribe;

public class ScoreLevelLabelSystem {
  private Player p;

  private final Label scoreLabel;
  private final Label levelLabel;

  public ScoreLevelLabelSystem(Label scoreLabel, Label levelLabel) {
    this.scoreLabel = scoreLabel;
    this.levelLabel = levelLabel;
  }

  @Subscribe
  public void onStart(GameStartEvent e) {
    p = TetrisGame.getInstance().getPlayer();
    updateLevelAndScore();
  }

  @Subscribe
  public void onScoreChange(ChangeScoreEvent e) {
    updateLevelAndScore();
  }

  @Subscribe
  public void onNextLevel(NextLevelEvent e) {
    updateLevelAndScore();
  }

  private void updateLevelAndScore() {
    scoreLabel.setText(String.format("%016d", p.getScore()));
    levelLabel.setText(String.format("%02d", p.getLevel()));
  }
}

package fr.univ_fcomte.iut_bm.tetris.game.events;

import javafx.scene.input.KeyCode;

public class KeyStateChangeEvent {

  private final KeyCode key;
  private final boolean pressed;

  public KeyStateChangeEvent(KeyCode key, boolean pressed) {
    this.key = key;
    this.pressed = pressed;
  }

  public KeyCode getKey() {
    return key;
  }

  public boolean isPressed() {
    return pressed;
  }
}

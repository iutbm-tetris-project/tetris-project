package fr.univ_fcomte.iut_bm.tetris.game;

import javafx.scene.canvas.GraphicsContext;

public interface BlockRenderer {

  void renderBlocks(GraphicsContext gc);

  void renderColor(GraphicsContext gc);
}

package fr.univ_fcomte.iut_bm.tetris.game.enums;

import com.google.gson.annotations.SerializedName;

public enum Command {
  @SerializedName("left")
  LEFT(CommandType.TRANSLATION, "Left"),

  @SerializedName("right")
  RIGHT(CommandType.TRANSLATION, "Right"),

  @SerializedName("down")
  DOWN(CommandType.TRANSLATION, "Down"),

  @SerializedName("rotate_back")
  ROTATE_BACK(CommandType.ACTION, "Rotate clockwise"),

  @SerializedName("rotate")
  ROTATE(CommandType.ACTION, "Rotate anti-clockwise"),

  @SerializedName("hold")
  HOLD(CommandType.ACTION, "Hold"),

  @SerializedName("drop")
  DROP(CommandType.ACTION, "Drop"),

  @SerializedName("activate")
  ACTIVATE_BM(CommandType.ACTION, "Activate");

  private final CommandType type;
  private final String name;

  Command(CommandType type, String name) {
    this.type = type;
    this.name = name;
  }

  public CommandType getType() {
    return type;
  }

  public String getName() {
    return name;
  }
}

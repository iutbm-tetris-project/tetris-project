package fr.univ_fcomte.iut_bm.tetris.game;

import static fr.univ_fcomte.iut_bm.tetris.Constants.BLOCK_IMAGE;
import static fr.univ_fcomte.iut_bm.tetris.Constants.BLOCK_SIZE;
import static fr.univ_fcomte.iut_bm.tetris.game.components.Field.FIELD_HEIGHT;

import com.google.common.collect.Lists;
import fr.univ_fcomte.iut_bm.tetris.game.components.Piece;
import fr.univ_fcomte.iut_bm.tetris.game.components.Point;
import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javafx.geometry.Point2D;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class Utilities {

  public static final int IMAGE_SIZE = 128;

  public static Point[] buildShape(int... points) {
    ArrayList<Integer> list = Lists.newArrayList(0, 0);
    Arrays.stream(points).forEach(list::add);

    return Lists.partition(list, 2)
        .stream()
        .map(x -> new Point(x.get(0), x.get(1)))
        .toArray(Point[]::new);
  }

  public static Point2D translateToRealPoint(int x, int y) {
    return new Point2D(x * BLOCK_SIZE, ((FIELD_HEIGHT - y - 1) * BLOCK_SIZE));
  }

  public static Point[] normalizedShape(Point[] shape) {
    shape = shape.clone();

    int shiftX = Math.abs(
        Arrays.stream(shape)
          .mapToInt(Point::getX)
          .filter(i -> i < 0)
          .min()
          .orElse(0)
    );

    int shiftY = Math.abs(
        Arrays.stream(shape)
            .mapToInt(Point::getY)
            .filter(i -> i < 0)
            .min()
            .orElse(0)
    );

    return Arrays.stream(shape)
        .map(p -> p.add(shiftX, shiftY))
        .toArray(Point[]::new);
  }

  public static int calculateWidth(Point[] shape) {
    return Arrays.stream(normalizedShape(shape))
        .mapToInt(p -> p.getX() + 1)
        .max()
        .orElseThrow();
  }

  public static int calculateHeight(Point[] shape) {
    return Arrays.stream(normalizedShape(shape))
        .mapToInt(p -> p.getY() + 1)
        .max()
        .orElseThrow();
  }

  public static Point[] flipShapeVertical(Point[] shape) {
    int y = calculateHeight(shape) - 1;

    return Arrays.stream(shape)
        .map(p -> p.subtract(0, y).abs())
        .toArray(Point[]::new);
  }

  public static WritableImage renderImage(PieceEnum p) {
    return renderImage(p.getShape(), p.getColor());
  }

  public static WritableImage renderImage(Piece p) {
    return renderImage(p.getShape(), p.getColor());
  }

  public static WritableImage renderImage(Point[] shape, Color color) {
    shape = normalizedShape(shape);
    shape = flipShapeVertical(shape);

    int blockSize = (int) BLOCK_SIZE;

    WritableImage image = new WritableImage(IMAGE_SIZE, IMAGE_SIZE);

    PixelWriter writer = image.getPixelWriter();
    PixelReader source = BLOCK_IMAGE.getPixelReader();

    int startX = (IMAGE_SIZE / 2) - ((calculateWidth(shape) * 32) / 2);
    int startY = (IMAGE_SIZE / 2) - ((calculateHeight(shape) * 32) / 2);

    for (Point p : shape) {
      int x = p.getX() * blockSize;
      int y = p.getY() * blockSize;

      for (int xi = 0; xi < blockSize; xi++)  {
        for (int yi = 0; yi < blockSize; yi++) {
          Color sourceColor = source.getColor(xi, yi);
          Color newColor = new Color(
              sourceColor.getRed() * color.getRed(),
              sourceColor.getBlue() * color.getGreen(),
              sourceColor.getGreen() * color.getBlue(),
              1d
          );
          writer.setColor(startX + x + xi, startY + y + yi, newColor);
        }
      }
    }

    return image;
  }

  public static <K, V> Map<V, K> inverseMap(Map<K, V> map) {
    return map
        .entrySet()
        .stream()
        .collect(Collectors.toMap(Entry::getValue, Entry::getKey));
  }
}

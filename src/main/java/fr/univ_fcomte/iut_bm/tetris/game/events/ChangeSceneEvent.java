package fr.univ_fcomte.iut_bm.tetris.game.events;

public class ChangeSceneEvent {

  private String sceneName;
  private Object data;

  public ChangeSceneEvent(String sceneName, Object data) {
    this.sceneName = sceneName;
    this.data = data;
  }

  public ChangeSceneEvent(String sceneName) {
    this(sceneName, null);
  }

  public String getSceneName() {
    return sceneName;
  }

  public void setSceneName(String sceneName) {
    this.sceneName = sceneName;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }
}

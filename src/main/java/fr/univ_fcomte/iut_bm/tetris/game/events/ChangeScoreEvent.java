package fr.univ_fcomte.iut_bm.tetris.game.events;

public class ChangeScoreEvent {
  private final int oldScore;
  private final int linesCleared;
  private int newScore;

  public ChangeScoreEvent(int oldScore, int linesCleared, int newScore) {
    this.oldScore = oldScore;
    this.linesCleared = linesCleared;
    this.newScore = newScore;
  }

  public int getOldScore() {
    return oldScore;
  }

  public int getLinesCleared() {
    return linesCleared;
  }

  public int getNewScore() {
    return newScore;
  }

  public void setNewScore(int newScore) {
    this.newScore = newScore;
  }

  public int diff() {
    return newScore - oldScore;
  }
}

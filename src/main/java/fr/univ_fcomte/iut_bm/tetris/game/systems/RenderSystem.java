package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.components.Field;
import fr.univ_fcomte.iut_bm.tetris.game.components.MovablePiece;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameUpdateEvent;
import javafx.scene.canvas.GraphicsContext;
import org.greenrobot.eventbus.Subscribe;

public class RenderSystem {

  public static final double CANVAS_WIDTH = 320;
  public static final double CANVAS_HEIGHT = 640;

  private GraphicsContext blockGC;
  private GraphicsContext colorGC;

  public RenderSystem(GraphicsContext blockGC, GraphicsContext colorGC) {
    this.blockGC = blockGC;
    this.colorGC = colorGC;
  }

  @Subscribe
  public void onGameUpdate(GameUpdateEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();

    Field field = player.getField();
    MovablePiece currentPiece = player.getPiece();
    MovablePiece ghostPiece = new MovablePiece(currentPiece);
    ghostPiece.drop();

    blockGC.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    colorGC.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

    field.renderBlocks(blockGC);
    field.renderColor(colorGC);
    currentPiece.renderBlocks(blockGC);
    currentPiece.renderColor(colorGC);
    ghostPiece.renderColor(colorGC);
  }
}

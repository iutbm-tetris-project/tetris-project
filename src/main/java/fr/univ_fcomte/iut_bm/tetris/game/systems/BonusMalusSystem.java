package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.bonus_malus.*;
import fr.univ_fcomte.iut_bm.tetris.game.bonus_malus.Double;
import fr.univ_fcomte.iut_bm.tetris.game.bonus_malus.Freeze;
import fr.univ_fcomte.iut_bm.tetris.game.bonus_malus.WeirdPiece;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import fr.univ_fcomte.iut_bm.tetris.game.events.ActionEvent;
import fr.univ_fcomte.iut_bm.tetris.game.bonus_malus.Reverse;
import fr.univ_fcomte.iut_bm.tetris.game.events.BonusActivateEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.BonusReadyEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameStopEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameUpdateEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.LineClearEvent;
import fr.univ_fcomte.iut_bm.tetris.game.primitives.BonusMalusEntry;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class BonusMalusSystem {
  private static final List<BonusMalusEntry> BONUS_MALUSES = Arrays.asList(
      new BonusMalusEntry("Freeze", "freeze.png", Freeze.class, 30 * 60),
      new BonusMalusEntry("Double", "double.png", Double.class, 30 * 60),
      new BonusMalusEntry("Reverse", "reverse.png", Reverse.class, 30*60),
      new BonusMalusEntry("Blind", "blind.png", Blind.class, 30*60),
      new BonusMalusEntry("Delete", "delete.png", DeleteLine.class, 10*60),
      new BonusMalusEntry("Speed", "speed.png", Speed.class, 30*60),
      new BonusMalusEntry("Weird Piece", "weird_pieces.png", WeirdPiece.class, 30*60)
  );

  private Random random;

  private final ImageView bonusMalusBox;
  private Optional<BonusMalus> currentBonusMalus;
  private Optional<BonusMalusEntry> selectedEntry;

  private int removeFrame;

  private boolean locked;

  public BonusMalusSystem(ImageView bonusMalusBox) {
    this.bonusMalusBox = bonusMalusBox;

    currentBonusMalus = Optional.empty();
    selectedEntry = Optional.empty();
    random = new Random();
    removeFrame = 0;

    lock();
  }

  public void activate(BonusMalusEntry entry, int currentFrame) {
    BonusMalus bonusMalus = entry.constructBonusMalus();

    currentBonusMalus = Optional.of(bonusMalus);
    removeFrame = currentFrame + entry.getDuration();
    bonusMalusBox.setImage(getImage(entry.getIcon()));

    bonusMalus.onEnable();

    EventBus.getDefault().post(new BonusActivateEvent());
  }

  public void deactivate() {
    if (currentBonusMalus.isEmpty()) {
      return;
    }

    BonusMalus bm = currentBonusMalus.get();
    bm.onDisable();

    currentBonusMalus = Optional.empty();
    lock();
  }

  public void lock() {
    locked = true;
    bonusMalusBox.setImage(null);
  }

  public void unlock() {
    locked = false;
    bonusMalusBox.setImage(getImage("random.png"));
    EventBus.getDefault().post(new BonusReadyEvent());
  }

  public Image getImage(String name) {
    return new Image(getClass().getResource(String.format("/fr/univ_fcomte/iut_bm/tetris/textures/bonus_malus/%s", name)).toString());
  }

  @Subscribe
  public void onGameUpdate(GameUpdateEvent e) {
    if (e.getFrameCount() >= removeFrame) {
      deactivate();
    }

    if (selectedEntry.isPresent()) {
      activate(selectedEntry.get(), e.getFrameCount());
      selectedEntry = Optional.empty();
    }
  }

  @Subscribe
  public void onLineClear(LineClearEvent e) {
    if (!locked) {
      return;
    }

    if (e.getLineCount() >= 4) {
      unlock();
    }
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onAction(ActionEvent e) {
    if (e.getCommand() != Command.ACTIVATE_BM)
      return;

    if (locked)
      return;

    if (currentBonusMalus.isPresent())
      return;

    if (selectedEntry.isPresent())
      return;

    selectedEntry = Optional.of(BONUS_MALUSES.get(random.nextInt(BONUS_MALUSES.size())));
  }

  @Subscribe
  public void onGameStop(GameStopEvent e) {
    deactivate();
  }

}

package fr.univ_fcomte.iut_bm.tetris.game.events;

import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.List;

public class NextPieceEvent {

  private List<PieceEnum> pieces;
  private PieceEnum nextPiece;

  public NextPieceEvent(List<PieceEnum> pieces, PieceEnum nextPiece) {
    this.pieces = pieces;
    this.nextPiece = nextPiece;
  }

  public List<PieceEnum> getPieces() {
    return pieces;
  }

  public void setPieces(List<PieceEnum> pieces) {
    this.pieces = pieces;
  }

  public PieceEnum getNextPiece() {
    return nextPiece;
  }

  public void setNextPiece(PieceEnum nextPiece) {
    this.nextPiece = nextPiece;
  }
}

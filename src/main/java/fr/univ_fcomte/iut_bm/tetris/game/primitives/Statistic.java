package fr.univ_fcomte.iut_bm.tetris.game.primitives;

public class Statistic {
  private final int score;
  private final int level;

  public Statistic(int score, int level) {
    this.score = score;
    this.level = level;
  }

  public int getScore() {
    return score;
  }

  public int getLevel() {
    return level;
  }
}

package fr.univ_fcomte.iut_bm.tetris.game.bonus_malus;

import static fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum.TETRIS_PIECES;

import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextPieceEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Random;

public class Blind extends BonusMalus {

  private Random random;

  @Override
  public void onEnable() {
    random = new Random();
    EventBus.getDefault().register(this);
  }

  @Override
  public void onDisable() {
    EventBus.getDefault().unregister(this);
  }

  @Subscribe(priority = EventPriority.HIGHER)
  public void onNextPiece(NextPieceEvent e) {
    e.setNextPiece(TETRIS_PIECES[random.nextInt(TETRIS_PIECES.length)]);
  }
}

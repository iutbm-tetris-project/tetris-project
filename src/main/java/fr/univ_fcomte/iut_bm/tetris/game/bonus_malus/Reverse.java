package fr.univ_fcomte.iut_bm.tetris.game.bonus_malus;

import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import fr.univ_fcomte.iut_bm.tetris.game.events.ActionEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.MoveEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class Reverse extends BonusMalus {

  @Override
  public void onEnable() {
    EventBus.getDefault().register(this);
  }

  @Override
  public void onDisable() {
    EventBus.getDefault().unregister(this);
  }

  @Subscribe(priority = EventPriority.HIGHEST)
  public void onMove(MoveEvent e) {
    switch (e.getCommand()) {
      case LEFT:
        e.setCommand(Command.RIGHT);
        break;
      case RIGHT:
        e.setCommand(Command.LEFT);
        break;
    }
  }

  @Subscribe(priority = EventPriority.HIGHEST)
  public void onAction(ActionEvent e) {
    switch (e.getCommand()) {
      case ROTATE:
        e.setCommand(Command.ROTATE_BACK);
        break;
      case ROTATE_BACK:
        e.setCommand(Command.ROTATE);
        break;
    }
  }
}

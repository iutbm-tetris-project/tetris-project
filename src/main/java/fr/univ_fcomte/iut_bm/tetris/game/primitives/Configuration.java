package fr.univ_fcomte.iut_bm.tetris.game.primitives;

import fr.univ_fcomte.iut_bm.tetris.game.generators.Generator;

public class Configuration {
  private final Class<? extends Generator> generatorClass;
  private final long seed;
  private final int levelStart;

  public Configuration(Class<? extends Generator> generatorClass, long seed, int levelStart) {
    this.generatorClass = generatorClass;
    this.seed = seed;
    this.levelStart = levelStart;
  }

  public Class<? extends Generator> getGeneratorClass() {
    return generatorClass;
  }

  public long getSeed() {
    return seed;
  }

  public int getLevelStart() {
    return levelStart;
  }
}

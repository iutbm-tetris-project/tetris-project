package fr.univ_fcomte.iut_bm.tetris.game.bonus_malus;

import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeScoreEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class Double extends BonusMalus {

  @Override
  public void onEnable() {
    EventBus.getDefault().register(this);
  }

  @Override
  public void onDisable() {
    EventBus.getDefault().unregister(this);
  }

  @Subscribe(priority = EventPriority.HIGHEST)
  public void onScoreChange(ChangeScoreEvent e) {
    int score = e.diff() * 2;
    e.setNewScore(e.getOldScore() + score);
  }
}

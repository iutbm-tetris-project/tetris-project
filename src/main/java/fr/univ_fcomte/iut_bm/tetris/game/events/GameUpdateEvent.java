package fr.univ_fcomte.iut_bm.tetris.game.events;

import java.util.ArrayList;
import javafx.scene.input.KeyCode;

public class GameUpdateEvent {

  private final int frameCount;
  private final ArrayList<KeyCode> keyStates;

  public GameUpdateEvent(int frameCount, ArrayList<KeyCode> keyStates) {
    this.frameCount = frameCount;
    this.keyStates = keyStates;
  }

  public int getFrameCount() {
    return frameCount;
  }

  public ArrayList<KeyCode> getKeyStates() {
    return keyStates;
  }
}

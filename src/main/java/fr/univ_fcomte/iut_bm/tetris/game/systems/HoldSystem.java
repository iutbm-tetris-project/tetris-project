package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.Utilities;
import fr.univ_fcomte.iut_bm.tetris.game.components.Piece;
import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import fr.univ_fcomte.iut_bm.tetris.game.events.ActionEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.InvokeNextPieceEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.MoveEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextPieceEvent;
import javafx.scene.image.ImageView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class HoldSystem {
  private Piece hold;
  private boolean hasHold = false;

  private final ImageView holdBoxImageView;

  public HoldSystem(ImageView holdBoxImageView) {
    this.holdBoxImageView = holdBoxImageView;
  }

  @Subscribe
  public void onMove(ActionEvent e) {
    if (e.getCommand() == Command.HOLD)
      hold();
  }

  @Subscribe
  public void onNextPiece(NextPieceEvent e) {
    hasHold = false;
  }

  private void hold() {
    Player p = TetrisGame.getInstance().getPlayer();

    if (hasHold)
      return;

    if (hold == null) {
      hold = p.getPiece();
      EventBus.getDefault().post(new InvokeNextPieceEvent());
    } else {
      Piece temp = p.getPiece();
      p.setPiece(hold);
      hold = temp;
      hasHold = true;
    }

    holdBoxImageView.setImage(Utilities.renderImage(hold));
  }
}

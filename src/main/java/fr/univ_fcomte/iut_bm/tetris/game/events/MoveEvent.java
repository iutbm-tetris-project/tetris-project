package fr.univ_fcomte.iut_bm.tetris.game.events;

import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;
import java.util.ArrayList;
import javafx.scene.input.KeyCode;

public class MoveEvent {

  private final int frameCount;
  private Command command;
  private final ArrayList<KeyCode> keyStates;

  public MoveEvent(int frameCount, Command command, ArrayList<KeyCode> keyStates) {
    this.frameCount = frameCount;
    this.command = command;
    this.keyStates = keyStates;
  }

  public int getFrameCount() {
    return frameCount;
  }

  public Command getCommand() {
    return command;
  }

  public void setCommand(Command command) {
    this.command = command;
  }

  public ArrayList<KeyCode> getKeyStates() {
    return keyStates;
  }
}

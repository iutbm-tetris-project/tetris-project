package fr.univ_fcomte.iut_bm.tetris.game.enums;

public enum CommandType {
  TRANSLATION,
  ACTION
}

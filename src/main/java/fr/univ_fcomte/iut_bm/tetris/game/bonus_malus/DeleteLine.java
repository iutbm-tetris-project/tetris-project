package fr.univ_fcomte.iut_bm.tetris.game.bonus_malus;

import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.components.Field;
import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import java.util.Random;

public class DeleteLine extends BonusMalus {

  private Random random;

  @Override
  public void onEnable() {
    random = new Random();

    Player player = TetrisGame.getInstance().getPlayer();
    Field field = player.getField();

    int count = 1 + random.nextInt(4);

    for (int i = 0; i < count; i++) {
      field.clearLine(i);
    }

    field.gravity();
  }

  @Override
  public void onDisable() {
  }
}

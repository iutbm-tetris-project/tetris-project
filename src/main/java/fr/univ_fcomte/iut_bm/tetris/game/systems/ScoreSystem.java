package fr.univ_fcomte.iut_bm.tetris.game.systems;

import static fr.univ_fcomte.iut_bm.tetris.Constants.LINE_POINTS;

import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeScoreEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.LineClearEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ScoreSystem {

  @Subscribe
  public void onLineClear(LineClearEvent e) {
    Player p = TetrisGame.getInstance().getPlayer();

    int level = p.getLevel();

    int oldScore = p.getScore();
    int linesCleared = e.getLineCount();
    int newScore = oldScore + calculateScore(linesCleared, level);

    EventBus.getDefault().post(new ChangeScoreEvent(oldScore, linesCleared, newScore));
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onChangeScore(ChangeScoreEvent e) {
    Player p = TetrisGame.getInstance().getPlayer();
    p.setScore(e.getNewScore());
  }

  private int calculateScore(int linesCleared, int level) {
    linesCleared = Math.min(linesCleared, LINE_POINTS.length - 1);
    return LINE_POINTS[linesCleared] * (level + 1);
  }
}

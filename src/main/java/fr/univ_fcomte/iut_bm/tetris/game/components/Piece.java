package fr.univ_fcomte.iut_bm.tetris.game.components;

import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.Arrays;
import java.util.Objects;
import javafx.scene.paint.Color;

public class Piece {

  protected Point[] shape;
  protected Color col;
  protected boolean spin;

  public Piece() {
  }

  public Piece(Piece b) {
    this(b.shape, b.col, b.spin);
  }

  public Piece(Point[] shape, Color col) {
    this.shape = shape.clone();
    this.col = col;
    this.spin = true;
  }

  public Piece(Point[] shape, Color col, boolean spin){
    this.shape = shape.clone();
    this.col = col;
    this.spin = spin;
  }

  public Piece(PieceEnum type) {
    this(type.getShape(), type.getColor(), type.getSpin());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Piece piece = (Piece) o;
    return Arrays.equals(shape, piece.shape) &&
        Objects.equals(col, piece.col);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(col);
    result = 31 * result + Arrays.hashCode(shape);
    return result;
  }

  public Point[] getShape() {
    return shape;
  }

  public void setShape(Point[] shape) {
    this.shape = shape;
  }

  public Color getColor() {
    return col;
  }

  public void setColor(Color col) {
    this.col = col;
  }
}

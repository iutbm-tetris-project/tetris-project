package fr.univ_fcomte.iut_bm.tetris.game.enums;

import fr.univ_fcomte.iut_bm.tetris.game.Utilities;
import fr.univ_fcomte.iut_bm.tetris.game.components.Piece;
import fr.univ_fcomte.iut_bm.tetris.game.components.Point;
import javafx.scene.paint.Color;


public enum PieceEnum {
  L(Utilities.buildShape(-1, 0, 1, 0, 1, 1), Color.web("#ff6d00")),
  J(Utilities.buildShape(-1, 1, -1, 0, 1, 0), Color.web("#304ffe")),
  S(Utilities.buildShape(-1, 0, 0, 1, 1, 1), Color.web("#00c853")),
  Z(Utilities.buildShape(-1, 1, 0, 1, 1, 0), Color.web("#d50000")),
  I(Utilities.buildShape(-1, 0, 1, 0, 2, 0), Color.web("#40c4ff")),
  O(Utilities.buildShape(0, 1, 1, 0, 1, 1), Color.web("#ffd600"), false),
  T(Utilities.buildShape(-1, 0, 0, 1, 1, 0), Color.web("#aa00ff")),

  U(Utilities.buildShape(-1, 0, -1, 1, 1, 0, 1, 1), Color.web("#311E10")),
  BIRD(Utilities.buildShape(-1, -1, 1, 0, 0, -1), Color.web("#512DA8")),
  LONG_T(Utilities.buildShape(-1, 1, 0, 1, 1, 1, 0, -1), Color.web("#CDDC39")),
  PLUS(Utilities.buildShape(-1, 0, 1, 0, 0, -1, 0, 1), Color.web("#F4511E")),
  MINUS(Utilities.buildShape(-1, 0, 1, 0), Color.web("#9CCC65")),
  TIMES(Utilities.buildShape(-1, -1, -1, 1, 1, 1, 1, -1), Color.web("#26C6DA")),
  N(Utilities.buildShape(1, 0, 0, -1, 0, -2, 2, -1, 2, -2), Color.web("#FFB300"));

  public static final PieceEnum[] TETRIS_PIECES = new PieceEnum[]{
      I,
      J,
      L,
      O,
      S,
      T,
      Z
  };

  public static final PieceEnum[] WEIRD_PIECES = new PieceEnum[]{
      U,
      BIRD,
      LONG_T,
      PLUS,
      MINUS,
      TIMES,
      N
  };

  private Point[] shape;
  private Color color;
  private boolean spin;

  PieceEnum(Point[] shape, Color color) {
    this.shape = shape;
    this.color = color;
    this.spin = true;
  }

  PieceEnum(Point[] shape, Color color, boolean spin){
    this.shape = shape;
    this.color = color;
    this.spin = spin;
  }

  public Point[] getShape() {
    return shape;
  }

  public Color getColor() {
    return color;
  }

  public boolean getSpin(){
    return spin;
  }

  public Piece createBlock() {
    return new Piece(this);
  }
}

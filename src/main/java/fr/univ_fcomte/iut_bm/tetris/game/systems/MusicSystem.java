package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameStartEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameStopEvent;
import java.net.URL;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import org.greenrobot.eventbus.Subscribe;

public class MusicSystem {
  public static final URL MUSIC_URL = MusicSystem.class
      .getResource("/fr/univ_fcomte/iut_bm/tetris/music/music.mp3");

  private MediaPlayer player;

  @Subscribe(priority = EventPriority.LOWEST)
  public void onStart(GameStartEvent e) {
    player = new MediaPlayer(new Media(MUSIC_URL.toString()));
    player.setOnEndOfMedia(() -> player.seek(Duration.ZERO));
    player.play();
  }

  @Subscribe
  public void onStop(GameStopEvent e) {
    player.stop();
  }
}

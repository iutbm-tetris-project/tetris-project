package fr.univ_fcomte.iut_bm.tetris.game.components;

import static fr.univ_fcomte.iut_bm.tetris.game.Utilities.translateToRealPoint;

import java.util.Objects;
import javafx.geometry.Point2D;

public class Point {

  private int x, y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public Point(Point p) {
    this(p.x, p.y);
  }

  public Point() {
    this(0, 0);
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Point point = (Point) o;
    return x == point.x &&
        y == point.y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  public Point add(int x, int y) {
    Point res = new Point(this);
    res.x += x;
    res.y += y;
    return res;
  }

  public Point add(Point o) {
    return add(o.x, o.y);
  }

  public Point subtract(int x, int y) {
    Point res = new Point(this);
    res.x -= x;
    res.y -= y;
    return res;
  }

  public Point subtract(Point o) {
    return subtract(o.x, o.y);
  }

  public Point mul(int x, int y) {
    Point res = new Point(this);
    res.x *= x;
    res.y *= y;
    return res;
  }

  public Point div(int x, int y) {
    Point res = new Point(this);
    res.x /= x;
    res.y /= y;
    return res;
  }

  public Point abs() {
    return new Point(Math.abs(x), Math.abs(y));
  }

  @Override
  public String toString() {
    return "Point{" +
        "x=" + x +
        ", y=" + y +
        '}';
  }

  public Point2D toPoint2D() {
    return translateToRealPoint(x, y);
  }
}

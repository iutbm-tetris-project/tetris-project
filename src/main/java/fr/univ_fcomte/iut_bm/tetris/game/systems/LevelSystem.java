package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextLevelEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeScoreEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class LevelSystem {

  private int clearedLines;

  public LevelSystem() {
    clearedLines = 0;
  }

  @Subscribe
  public void onScoreChange(ChangeScoreEvent e) {
    clearedLines += e.getLinesCleared();

    int level = TetrisGame.getInstance().getPlayer().getLevel();
    int nbLineToClear =
        Math.min(Math.min((level + 1) * 10, Math.max(100, level * 10 - 50)), 200);

    if (nbLineToClear >= clearedLines) {
      EventBus.getDefault().post(new NextLevelEvent());
      clearedLines = 0;
    }
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onNextLevel(NextLevelEvent e) {
    TetrisGame.getInstance().getPlayer().nextLevel();
  }
}

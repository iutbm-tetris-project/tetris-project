package fr.univ_fcomte.iut_bm.tetris.game.generators;

import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Random;

public abstract class Generator {

  protected Random random;

  public Generator(long seed) {
    random = new Random(seed);
  }

  public abstract List<PieceEnum> generateBlocks();
}

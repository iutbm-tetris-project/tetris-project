package fr.univ_fcomte.iut_bm.tetris.game.systems;

import fr.univ_fcomte.iut_bm.tetris.game.events.GameUpdateEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.KeyStateChangeEvent;
import java.util.ArrayList;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class GameLoopSystem {

  public static final double TICK_PER_SECOND = 60d;
  public static final double TICK_DURATION = 1 / TICK_PER_SECOND;

  private final Timeline timeline;

  private int frameCounter = 0;
  private final ArrayList<KeyCode> keyStates;

  public GameLoopSystem() {
    timeline = new Timeline();
    timeline.getKeyFrames().add(
        new KeyFrame(Duration.seconds(TICK_DURATION), this::tick));
    timeline.setCycleCount(Timeline.INDEFINITE);
    keyStates = new ArrayList<>();
  }

  private void tick(ActionEvent e) {
    EventBus.getDefault().post(new GameUpdateEvent(frameCounter, keyStates));
    frameCounter++;
  }

  public void play() {
    timeline.play();
  }

  public void stop() {
    timeline.stop();
  }

  @Subscribe
  public void onKeyStateChange(KeyStateChangeEvent e) {
    KeyCode key = e.getKey();

    if (e.isPressed()) {
      if (!keyStates.contains(key)) {
        keyStates.add(key);
      }
    } else {
      keyStates.remove(key);
    }
  }
}

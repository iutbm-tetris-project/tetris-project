package fr.univ_fcomte.iut_bm.tetris.game.events;

public class LineClearEvent {
  private int lineCount;

  public LineClearEvent(int lineCount) {
    this.lineCount = lineCount;
  }

  public int getLineCount() {
    return lineCount;
  }

  public void setLineCount(int lineCount) {
    this.lineCount = lineCount;
  }
}

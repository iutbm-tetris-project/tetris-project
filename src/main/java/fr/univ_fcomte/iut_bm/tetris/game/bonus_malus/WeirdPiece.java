package fr.univ_fcomte.iut_bm.tetris.game.bonus_malus;

import static fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum.TETRIS_PIECES;
import static fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum.WEIRD_PIECES;

import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.components.Piece;
import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextPieceEvent;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class WeirdPiece extends BonusMalus {
  private Random random;

  @Override
  public void onEnable() {
    random = new Random();
    replacePieces();
    EventBus.getDefault().register(this);
  }

  @Override
  public void onDisable() {
    EventBus.getDefault().unregister(this);
    restorePieces();
  }

  @Subscribe(priority = EventPriority.HIGHEST)
  public void onNextPiece(NextPieceEvent e) {
    e.setPieces(generateWeirdPieces());
  }

  public List<PieceEnum> generatePieces(PieceEnum[] pieces) {
    List<PieceEnum> list = Arrays.asList(pieces);
    Collections.shuffle(list, random);
    return list;
  }

  public List<PieceEnum> generateWeirdPieces() {
    return generatePieces(WEIRD_PIECES);
  }

  public List<PieceEnum> generateNormalPieces() {
    return generatePieces(TETRIS_PIECES);
  }

  public void replacePieces() {
    Player player = TetrisGame.getInstance().getPlayer();

    ArrayDeque<PieceEnum> pieces = player.getPieces();
    pieces.clear();

    pieces.addAll(generateWeirdPieces());
  }

  public void restorePieces() {
    Player player = TetrisGame.getInstance().getPlayer();

    ArrayDeque<PieceEnum> pieces = player.getPieces();
    pieces.clear();

    pieces.addAll(generateNormalPieces());
  }
}

package fr.univ_fcomte.iut_bm.tetris.game;

import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameStartEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameStopEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextPieceEvent;
import fr.univ_fcomte.iut_bm.tetris.game.generators.Generator;
import fr.univ_fcomte.iut_bm.tetris.game.systems.*;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class TetrisGame {

  private static TetrisGame theInstance;

  private GameLoopSystem gameLoopSystem;
  private InputSystem inputSystem;
  private DelayedAutoShiftSystem delayedAutoShiftSystem;
  private MovementSystem movementSystem;
  private RenderSystem renderSystem;
  private LogicSystem logicSystem;
  private ScoreSystem scoreSystem;
  private LevelSystem levelSystem;
  private NextBoxesSystem nextBoxesSystem;
  private HoldSystem holdSystem;
  private ScoreLevelLabelSystem scoreLevelLabelSystem;
  private BonusMalusSystem bonusMalusSystem;
  private MusicSystem musicSystem;
  private SoundSystem soundSystem;

  private Player player;

  private Generator generator;
  private int startLevel;

  public TetrisGame(Generator generator,
      int startLevel,
      ImageView[] nextBoxes,
      ImageView holdBox,
      ImageView bonusMalusBox,
      GraphicsContext blockGC,
      GraphicsContext colorGC,
      Label scoreLabel,
      Label levelLabel) {
    this.generator = generator;
    this.startLevel = startLevel;

    gameLoopSystem = new GameLoopSystem();
    inputSystem = new InputSystem();
    delayedAutoShiftSystem = new DelayedAutoShiftSystem();
    movementSystem = new MovementSystem();
    renderSystem = new RenderSystem(blockGC, colorGC);
    logicSystem = new LogicSystem();
    scoreSystem = new ScoreSystem();
    levelSystem = new LevelSystem();
    nextBoxesSystem = new NextBoxesSystem(nextBoxes);
    holdSystem = new HoldSystem(holdBox);
    scoreLevelLabelSystem = new ScoreLevelLabelSystem(scoreLabel, levelLabel);
    bonusMalusSystem = new BonusMalusSystem(bonusMalusBox);
    musicSystem = new MusicSystem();
    soundSystem = new SoundSystem();
  }


  public void start() {
    TetrisGame.setInstance(this);

    player = new Player(startLevel);

    EventBus.getDefault().register(this);
    EventBus.getDefault().register(gameLoopSystem);
    EventBus.getDefault().register(inputSystem);
    EventBus.getDefault().register(delayedAutoShiftSystem);
    EventBus.getDefault().register(movementSystem);
    EventBus.getDefault().register(renderSystem);
    EventBus.getDefault().register(logicSystem);
    EventBus.getDefault().register(scoreSystem);
    EventBus.getDefault().register(levelSystem);
    EventBus.getDefault().register(nextBoxesSystem);
    EventBus.getDefault().register(holdSystem);
    EventBus.getDefault().register(scoreLevelLabelSystem);
    EventBus.getDefault().register(bonusMalusSystem);
    EventBus.getDefault().register(musicSystem);
    EventBus.getDefault().register(soundSystem);

    EventBus.getDefault().post(new GameStartEvent());
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onStart(GameStartEvent e) {
    List<PieceEnum> pieces = generator.generateBlocks();
    PieceEnum piece = pieces.get(0);
    pieces = pieces.subList(1, pieces.size()-1);
    EventBus.getDefault().post(new NextPieceEvent(pieces, piece));

    gameLoopSystem.play();
  }

  public void stop() {
    EventBus.getDefault().post(new GameStopEvent());
  }

  @Subscribe(priority = EventPriority.LOWEST)
  public void onStop(GameStopEvent e) {
    gameLoopSystem.stop();

    EventBus.getDefault().unregister(this);
    EventBus.getDefault().unregister(gameLoopSystem);
    EventBus.getDefault().unregister(inputSystem);
    EventBus.getDefault().unregister(delayedAutoShiftSystem);
    EventBus.getDefault().unregister(movementSystem);
    EventBus.getDefault().unregister(renderSystem);
    EventBus.getDefault().unregister(logicSystem);
    EventBus.getDefault().unregister(scoreSystem);
    EventBus.getDefault().unregister(levelSystem);
    EventBus.getDefault().unregister(nextBoxesSystem);
    EventBus.getDefault().unregister(holdSystem);
    EventBus.getDefault().unregister(scoreLevelLabelSystem);
    EventBus.getDefault().unregister(bonusMalusSystem);
    EventBus.getDefault().unregister(musicSystem);
    EventBus.getDefault().unregister(soundSystem);

    player = null;
    TetrisGame.setInstance(null);
  }

  public Player getPlayer() {
    return player;
  }

  public static TetrisGame getInstance() {
    return theInstance;
  }

  public static void setInstance(TetrisGame game) {
    theInstance = game;
  }

  public Generator getGenerator() {
    return generator;
  }
}

package fr.univ_fcomte.iut_bm.tetris.game.constants;

public class EventPriority {

  public static final int HIGHEST = 3;
  public static final int HIGHER = 2;
  public static final int HIGH = 1;
  public static final int NORMAL = 0;
  public static final int LOW = -1;
  public static final int LOWER = -2;
  public static final int LOWEST = -3;
}

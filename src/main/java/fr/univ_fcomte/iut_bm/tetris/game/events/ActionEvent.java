package fr.univ_fcomte.iut_bm.tetris.game.events;

import fr.univ_fcomte.iut_bm.tetris.game.enums.Command;

public class ActionEvent {

  private Command command;

  public ActionEvent(Command command) {
    this.command = command;
  }

  public Command getCommand() {
    return command;
  }

  public void setCommand(Command command) {
    this.command = command;
  }
}

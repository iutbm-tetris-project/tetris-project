package fr.univ_fcomte.iut_bm.tetris.game.systems;

import static fr.univ_fcomte.iut_bm.tetris.Constants.FRAME_DELAYS;
import static fr.univ_fcomte.iut_bm.tetris.game.components.Field.FIELD_HEIGHT;
import static fr.univ_fcomte.iut_bm.tetris.game.components.Field.FILED_TRUE_HEIGHT;

import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.primitives.Statistic;
import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.components.Field;
import fr.univ_fcomte.iut_bm.tetris.game.components.MovablePiece;
import fr.univ_fcomte.iut_bm.tetris.game.constants.EventPriority;
import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import fr.univ_fcomte.iut_bm.tetris.game.events.ChangeSceneEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameOverEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GameUpdateEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.GravityEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.InvokeNextPieceEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.LineClearEvent;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextPieceEvent;
import fr.univ_fcomte.iut_bm.tetris.game.generators.Generator;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class LogicSystem {

  @Subscribe(priority = EventPriority.LOW)
  public void onTick(GameUpdateEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();

    Field field = player.getField();
    MovablePiece currentPiece = player.getPiece();

    int delayIndex = Math.min(player.getLevel(), 29);
    int delay = FRAME_DELAYS[delayIndex];

    if (e.getFrameCount() % delay == 0) {
      if (!currentPiece.tryToGoDown()) {
        currentPiece.attach();
        EventBus.getDefault().post(new InvokeNextPieceEvent());
      }
    }

    int lineCount = field.countLines();
    if (lineCount > 0)
      EventBus.getDefault().post(new LineClearEvent(lineCount));

    EventBus.getDefault().post(new GravityEvent());
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onGravity(GravityEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();
    Field field = player.getField();
    field.gravity();
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onLineClear(LineClearEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();
    Field field = player.getField();
    field.clearLines();
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onInvokeNextPiece(InvokeNextPieceEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();
    Generator generator = TetrisGame.getInstance().getGenerator();

    PieceEnum nextPiece = player.getPieces().poll();
    List<PieceEnum> pieces = generator.generateBlocks();

    EventBus.getDefault().post(new NextPieceEvent(pieces, nextPiece));
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onNextPiece(NextPieceEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();
    player.setPiece(e.getNextPiece());
    player.getPieces().addAll(e.getPieces());
  }

  @Subscribe(priority = EventPriority.LOWEST)
  public void checkNewPieceFits(NextPieceEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();

    if (!player.getPiece().fit())
      EventBus.getDefault().post(new GameOverEvent());
  }

  @Subscribe(priority = EventPriority.LOWEST)
  public void checkPieceOnHiddenLine(GameUpdateEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();

    for (int i = FIELD_HEIGHT + 1; i < FILED_TRUE_HEIGHT; i++)
      if (!player.getField().checkEmptyLine(FIELD_HEIGHT))
        EventBus.getDefault().post(new GameOverEvent());
  }

  @Subscribe(priority = EventPriority.HIGH)
  public void onGameOver(GameOverEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();

    Statistic stat = new Statistic(player.getScore(), player.getLevel());

    EventBus.getDefault().post(new ChangeSceneEvent("GameOver", stat));
  }
}

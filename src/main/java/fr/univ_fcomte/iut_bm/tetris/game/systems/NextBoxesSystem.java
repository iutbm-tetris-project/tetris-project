package fr.univ_fcomte.iut_bm.tetris.game.systems;


import fr.univ_fcomte.iut_bm.tetris.game.components.Player;
import fr.univ_fcomte.iut_bm.tetris.game.TetrisGame;
import fr.univ_fcomte.iut_bm.tetris.game.Utilities;
import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import fr.univ_fcomte.iut_bm.tetris.game.events.NextPieceEvent;
import javafx.scene.image.ImageView;
import org.greenrobot.eventbus.Subscribe;

public class NextBoxesSystem {
  private final ImageView[] nextBoxes;

  public NextBoxesSystem(ImageView[] nextBoxes) {
    this.nextBoxes = nextBoxes;
  }

  @Subscribe
  public void onNextPiece(NextPieceEvent e) {
    Player player = TetrisGame.getInstance().getPlayer();
    PieceEnum[] pieces = player.getPieces().toArray(s -> new PieceEnum[s]);

    for (int i = 0; i < nextBoxes.length; i++) {
      ImageView nextBox = nextBoxes[i];
      PieceEnum piece = pieces[i];
      nextBox.setImage(Utilities.renderImage(piece));
    }
  }
}

package fr.univ_fcomte.iut_bm.tetris.game.components;

import fr.univ_fcomte.iut_bm.tetris.game.enums.PieceEnum;
import java.util.ArrayDeque;

public class Player {

  private Field field;
  private MovablePiece piece;
  private ArrayDeque<PieceEnum> pieces;

  private int score;
  private int level;

  public Player(int level) {
    this.level = level;

    score = 0;
    field = new Field();
    pieces = new ArrayDeque<>();
  }

  public void nextLevel() {
    level++;
  }

  public Field getField() {
    return field;
  }

  public void setField(Field field) {
    this.field = field;
  }

  public MovablePiece getPiece() {
    return piece;
  }

  public void setPiece(MovablePiece piece) {
    this.piece = piece;
  }

  public void setPiece(Piece piece) {
    setPiece(new MovablePiece(piece, field.getSpawnPoint(), field));
  }

  public void setPiece(PieceEnum piece) {
    setPiece(new Piece(piece));
  }

  public ArrayDeque<PieceEnum> getPieces() {
    return pieces;
  }

  public void setPieces(ArrayDeque<PieceEnum> pieces) {
    this.pieces = pieces;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }
}

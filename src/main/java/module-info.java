module fr.univ_fcomte.iut_bm.tetris {
  requires javafx.base;
  requires javafx.controls;
  requires javafx.graphics;
  requires javafx.fxml;
  requires javafx.media;
  requires javafx.web;

  requires org.jfxtras.styles.jmetro;
  requires eventbus;
  requires com.google.gson;
  requires com.google.common;

  opens fr.univ_fcomte.iut_bm.tetris;

  opens fr.univ_fcomte.iut_bm.tetris.controllers to javafx.fxml;

  opens fr.univ_fcomte.iut_bm.tetris.configuration to com.google.gson;

  opens fr.univ_fcomte.iut_bm.tetris.game.systems to eventbus;
  opens fr.univ_fcomte.iut_bm.tetris.game to eventbus;
  opens fr.univ_fcomte.iut_bm.tetris.game.bonus_malus to eventbus;
}
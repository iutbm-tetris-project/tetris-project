# Tetris Project

Yet another Tetris clone written in Java with JavaFX

## Notice: Java 11+ is required

This projects **requires** at least **Java 11**. Java 8 and below **will not work**. 
You have been **warned**.

There may be some bugs on Java 13 and above. It is not recommended to use anything other than 
Java 11 LTS.

## Launching the game locally with Gradle

Open your terminal and navigate to the project folder.

Once you're inside, execute the following command:

```shell 
$ ./gradlew run
```

If you're on windows, do the following instead:

```batch
C:\> gradlew.bat run
```

## Setting up the local development environment

IntelliJ IDEA is highly suggested as it is aware of Java 9 modules, 
Gradle dependencies among other things.

To setup, simply click on "Import project" and select the `build.gradle` file.

## Compiling into a distributable Java Image

Simply run the `dist` task, like the following (with Gradle wrapper):

```shell
$ ./gradlew dist
```

Do note Java Image cannot do cross-compilation. So, if you want to build a Windows image, 
you need to compile on Windows. Same goes for macOS, Linux, etc.

## Known Bugs

### Sound not working

This is a known issue due to OpenJFX media module uses native libraries which uses local system 
media codeds. As of now, we're not sure what codecs are being used as it only returns an `UNKNOWN` 
media exception. Some code digging into the OpenJFX codebase is required...